package test.java.application;

import main.java.application.controllers.CalcOperation;

import org.junit.Assert;
import org.junit.Test;

public class CalcOperationTest {

	@Test
	public void testAddition() {
		double delta = 0;

		CalcOperation ope = CalcOperation.getInstance();
		Assert.assertEquals(111, ope.addition(85, 26), delta);
		Assert.assertEquals(-96.81569, ope.addition(-12.26589, -84.5498), delta);
		Assert.assertEquals(0, ope.addition(-98, 98), delta);
		Assert.assertEquals(26, ope.addition(12, 14), delta);
		Assert.assertEquals(1981462, ope.addition(9819, 1971643), delta);
		Assert.assertEquals(-80, ope.addition(0, -80), delta);
		Assert.assertEquals(1000, ope.addition(999, 1), delta);
	}

	@Test
	public void testSubtraction() {
		double delta = 0;
		CalcOperation ope = CalcOperation.getInstance();
		Assert.assertEquals(111, ope.subtraction(120, 9), delta);
		Assert.assertEquals(-216.81569, ope.subtraction(-156.81569, 60), delta);
		Assert.assertEquals(-196, ope.subtraction(-98, 98), delta);
		Assert.assertEquals(196, ope.subtraction(98, -98), delta);
		Assert.assertEquals(-2, ope.subtraction(12, 14), delta);
		Assert.assertEquals(-1961824, ope.subtraction(9819, 1971643), delta);
		Assert.assertEquals(80, ope.subtraction(0, -80), delta);
		Assert.assertEquals(998, ope.subtraction(999, 1), delta);
	}

	@Test
	public void testMultiplication() {
		double delta = 0;
		CalcOperation ope = CalcOperation.getInstance();
		Assert.assertEquals(125, ope.multiplication(5, 25), delta);
		Assert.assertEquals(411432, ope.multiplication(651, 632), delta);
		Assert.assertEquals(-9604, ope.multiplication(-98, 98), delta);
		Assert.assertEquals(168, ope.multiplication(12, 14), delta);
		Assert.assertEquals(19359562617D, ope.multiplication(9819, 1971643),
				delta);
		Assert.assertEquals(0, ope.multiplication(0, -80), delta);
		Assert.assertEquals(999, ope.multiplication(999, 1), delta);
	}

	@Test
	public void testDivision() {
		double delta = 0;
		CalcOperation ope = CalcOperation.getInstance();
		Assert.assertEquals(Double.NaN, ope.division(0, 0), delta);
		Assert.assertEquals(2, ope.division(10, 5), delta);
		Assert.assertEquals(-1, ope.division(-98, 98), delta);
		Assert.assertEquals(0.857, ope.division(12, 14), 0.001);
		Assert.assertEquals(0.004979, ope.division(9819, 1971643), 0.00001);
		Assert.assertEquals(0, ope.division(0, -80), delta);
		Assert.assertEquals(999, ope.division(999, 1), delta);
	}

	@Test
	public void testSquare() {
		double delta = 0.01;
		CalcOperation ope = CalcOperation.getInstance();
		Assert.assertEquals(968649.64, ope.square(-984.2), delta);
		Assert.assertEquals(3172.618, ope.square(-56.326), delta);
		Assert.assertEquals(4, ope.square(-2), delta);
		Assert.assertEquals(0, ope.square(0), delta);
		Assert.assertEquals(0.219, ope.square(0.468), delta);
		Assert.assertEquals(21213.9225, ope.square(145.65), delta);
		Assert.assertEquals(2.83888801E8, ope.square(16849), delta);

	}

	@Test
	public void testSquareRoot() {
		double delta = 0.0001;
		CalcOperation ope = CalcOperation.getInstance();
		Assert.assertEquals(Double.NaN, ope.squareRoot(-984.2), delta);
		Assert.assertEquals(Double.NaN, ope.squareRoot(-56.326), delta);
		Assert.assertEquals(Double.NaN, ope.squareRoot(-2), delta);
		Assert.assertEquals(0, ope.squareRoot(0), delta);
		Assert.assertEquals(0.6841, ope.squareRoot(0.468), delta);
		Assert.assertEquals(12.0685, ope.squareRoot(145.65), delta);
		Assert.assertEquals(129.8036, ope.squareRoot(16849), delta);
	}

	@Test
	public void testSetNumber() {
		CalcOperation ope = CalcOperation.getInstance();
		ope.addNumber("345.092331");
		Assert.assertEquals(1, ope.getNumbers().size(), 0);
		Assert.assertEquals(345.092331, ope.getNumbers().get(0), 0);
		ope.addNumber("486");
		Assert.assertEquals(2, ope.getNumbers().size(), 0);
		Assert.assertEquals(486, ope.getNumbers().get(1), 0);
	}

	@Test
	public void testSetSign() {
		CalcOperation ope = CalcOperation.getInstance();
		ope.addSign("+");
		Assert.assertEquals(1, ope.getSigns().size(), 0);
		Assert.assertTrue(ope.getSigns().get(0).equals("+"));
	}

	@Test
	public void testCalculateExpression() {

	}

	@Test
	public void testDoOperation() {

	}

	@Test
	public void testStoreAndExecute() {

	}

}
