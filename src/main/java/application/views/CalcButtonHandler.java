package main.java.application.views;

import main.java.application.controllers.CalcOperation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class CalcButtonHandler {

	@FXML
	Label display;

	String tempValue = "";

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {

	}

	/**
	 * Button square
	 */
	@FXML
	public void handleSquareButtonClick() {
		System.out.println("Button clicked");
	}

	/**
	 * Button Power
	 */
	@FXML
	public void handlePowButtonClick() {
		System.out.println("Button clicked");
	}

	/**
	 * Button square root
	 */
	@FXML
	public void handleSquareRootButtonClick() {
		System.out.println("Button clicked");
	}

	/**
	 * Button get back
	 */
	@FXML
	public void handleGetBackButtonClick() {
		System.out.println("Button clicked");
	}

	/**
	 * Button get forward
	 */
	@FXML
	public void handleGetForwardButtonClick() {
		System.out.println("Button clicked");
	}
	
	/**
	 * Button clear
	 */
	@FXML
	public void handleClearButtonClick() {
		CalcOperation ope = CalcOperation.getInstance();
		ope.clearNumbers();
		ope.clearSigns();
		tempValue = "";
		display.setText(tempValue);
	}
	
	/**
	 * 
	 * @param event
	 */
	@FXML
	public void handleClearMemoryButtonClick()
	{
		CalcOperation ope = CalcOperation.getInstance();
		ope.setMemory1(Double.MIN_VALUE);
		ope.setMemory2(Double.MIN_VALUE);
		ope.setMemory3(Double.MIN_VALUE);
	}

	@FXML
	public void handleMemoryButtonClick(ActionEvent event) {
		double number = Double.MIN_VALUE;

		if (event.getSource() instanceof Button) {
			CalcOperation ope = CalcOperation.getInstance();
			Button button = (Button) event.getSource();

			try {
				number = Double.valueOf(display.getText());
			} catch (NumberFormatException e) {

			}

			String textButton = button.getText();
			switch (textButton) {
			case "M1":
				if (ope.getMemory1() == Double.MIN_VALUE && number != Double.MIN_VALUE) {
					ope.setMemory1(number);
					display.setText("M1 stored");
				} else if (ope.getMemory1() == Double.MIN_VALUE) {
					display.setText("empty");
				} else {
					tempValue = tempValue + String.valueOf(ope.getMemory1());
					display.setText(display.getText() + tempValue);
				}
				break;
			case "M2":
				if (ope.getMemory2() == Double.MIN_VALUE && number != Double.MIN_VALUE) {
					ope.setMemory2(number);
					display.setText("M2 stored");
				} else if (ope.getMemory2() == Double.MIN_VALUE) {
					display.setText("empty");
				} else {
					tempValue = tempValue + String.valueOf(ope.getMemory2());
					display.setText(display.getText() + tempValue);
				}
				break;
			case "M3":
				if (ope.getMemory3() == Double.MIN_VALUE && number != Double.MIN_VALUE) {
					ope.setMemory3(number);
					display.setText("M3 stored");
				} else if (ope.getMemory3() == Double.MIN_VALUE) {
					display.setText("empty");
				} else {
					tempValue = tempValue + String.valueOf(ope.getMemory3());
					display.setText(display.getText() + tempValue);
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Button number
	 */
	@FXML
	public void handleNumberButtonClick(ActionEvent event) {
		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();

			String text = button.getText();

			tempValue = tempValue + text;

			display.setText(display.getText() + text);
		}
	}

	/**
	 * Button sign
	 */
	@FXML
	public void handleSignButtonClick(ActionEvent event) {
		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();

			String text = button.getText();

			if (false == tempValue.equals("")) {
				CalcOperation.getInstance().addNumber(tempValue);
				Double result = CalcOperation.getInstance()
						.calculateExpression(text);
				CalcOperation.getInstance().addSign(text);
				if (result != null) {
					display.setText(result.toString());
				}
				tempValue = "";

				display.setText(display.getText() + text);
			}
		}
	}

	/**
	 * Button equal
	 */
	@FXML
	public void handleEqualButtonClick(ActionEvent event) {
		if (event.getSource() instanceof Button) {
			Button button = (Button) event.getSource();

			String text = button.getText();

			if (false == tempValue.equals("")) {
				CalcOperation.getInstance().addNumber(tempValue);
				Double result = CalcOperation.getInstance()
						.calculateExpression(text);
				if (result != null) {
					display.setText(result.toString());
					tempValue = result.toString();
				}
			}
		}
	}
}
