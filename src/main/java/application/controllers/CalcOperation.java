package main.java.application.controllers;

import java.util.ArrayList;
import java.util.List;

public class CalcOperation {

	private static CalcOperation INSTANCE = null;

	private List<Command> commandsHistory = new ArrayList<Command>();

	private List<Double> numbers = new ArrayList<Double>();

	private List<String> signs = new ArrayList<String>();

	private double memory1 = Double.MIN_VALUE;
	private double memory2 = Double.MIN_VALUE;
	private double memory3 = Double.MIN_VALUE;
	
	public Double calculateExpression(String sign) {
		Double result = null;

		if (false == signs.isEmpty()) {
			if (signs.size() > 1) {
				result = doOperation(signs.get(1), numbers.get(1), numbers.get(2));
				numbers.remove(2);
				numbers.remove(1);
				signs.remove(1);
	
				numbers.add(result);
			}

			if ((signs.get(0).equals("*")) || (signs.get(0).equals("/"))
					|| (sign.equals("+")) || (sign.equals("-")) || (sign.equals("="))) {
				result = doOperation(signs.get(0), numbers.get(0),
						numbers.get(1));
				signs.remove(0);
				numbers.clear();

				if (false == sign.equals("=")) {
					numbers.add(result);
				}
			}
		}

		return result;
	}

	protected Double doOperation(String sign, Double number1, Double number2) {
		Double result = null;

		switch (sign) {
		case "+":
			result = addition(number1, number2);
			break;
		case "-":
			result = subtraction(number1, number2);
			break;
		case "*":
			result = multiplication(number1, number2);
			break;
		case "/":
			result = division(number1, number2);
			break;
		default:
			break;
		}

		return result;
	}

	private CalcOperation() {
	}

	/**
	 * Singleton instance
	 * 
	 * @return
	 */
	public static CalcOperation getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new CalcOperation();
		}
		return INSTANCE;
	}

	private Double storeAndExecute(Command command) {
		commandsHistory.add(command);
		command.execute();
		return command.getResult();
	}

	public double addition(double a, double b) {
		Command command = new AdditionCommand(a, b);

		Double result = storeAndExecute(command);

		return (result);
	}

	public double subtraction(double a, double b) {
		Command command = new SubtractionCommand(a, b);

		Double result = storeAndExecute(command);

		return result;
	}

	public double multiplication(double a, double b) {
		Command command = new MultiplicationCommand(a, b);

		Double result = storeAndExecute(command);

		return result;
	}

	public double division(double a, double b) {
		Command command = new DivisionCommand(a, b);

		Double result = storeAndExecute(command);

		return result;
	}

	public double square(double a) {
		return Math.pow(a, 2);
	}

	public double squareRoot(double a) {
		return Math.sqrt(a);
	}

	public List<Command> getCommandsHistory() {
		return commandsHistory;
	}

	public List<Double> getNumbers() {
		return numbers;
	}

	public List<String> getSigns() {
		return signs;
	}

	public List<Double> getNumbersOperations() {
		return numbers;
	}
	
	public double getMemory1() {
		return memory1;
	}

	public void setMemory1(double memory1) {
		this.memory1 = memory1;
	}

	public double getMemory2() {
		return memory2;
	}

	public void setMemory2(double memory2) {
		this.memory2 = memory2;
	}

	public double getMemory3() {
		return memory3;
	}

	public void setMemory3(double memory3) {
		this.memory3 = memory3;
	}
	
	public void setCommandsHistory(List<Command> commandsHistory) {
		this.commandsHistory = commandsHistory;
	}

	public void clearNumbers() {
		this.numbers.clear();
	}

	public void clearSigns() {
		this.signs.clear();
	}
	
	public void clearCommands()
	{
		this.commandsHistory.clear();
	}
	
	public void addNumber(String stringNumber) {
		Double number = Double.parseDouble(stringNumber);
		numbers.add(number);
	}

	public void addSign(String sign) {
		signs.add(sign);
	}
}
