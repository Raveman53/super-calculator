package main.java.application.controllers;

public interface Command {
	void execute();

	Double getResult();
}
