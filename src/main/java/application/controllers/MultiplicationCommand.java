package main.java.application.controllers;

/**
 * 
 * @author Fanch
 *
 */
public class MultiplicationCommand implements Command {
	private Double number1;
	private Double number2;
	private Double result;

	MultiplicationCommand(Double number1, Double number2) {
		this.number1 = number1;
		this.number2 = number2;
	}

	@Override
	public void execute() {
		result = number1 * number2;
	}

	public Double getNumber1() {
		return number1;
	}

	public Double getNumber2() {
		return number2;
	}

	public Double getResult() {
		return result;
	}

	public String toString() {
		return number1 + " * " + number2;
	}
}
